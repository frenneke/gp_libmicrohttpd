#! /usr/bin/env bash
set -e

git submodule update --init --recursive

MACHINE=`uname -m`_`uname -s`
CC=`which gcc`
CXX=`which g++`
export CC=$CC
export CXX=$CXX

mkdir -p build/$MACHINE
mkdir -p temp/$MACHINE

cd temp/$MACHINE


../../libmicrohttpd/configure --enable-stl --enable-monolithic --disable-shared --prefix=`pwd`/../../build/$MACHINE/
make -j4
make install